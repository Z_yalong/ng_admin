import {FormControl} from "@angular/forms";

export default class CustomValidators {

  static validNumber(c: FormControl) {
    if (!c.value) {
      return {
        validNumber: {
          valid: false,
          errorMsg: '请输入内容'
        }
      };
    }
    const Number_RegExp = /^[0-9]\d*$/;
    return (Number_RegExp.test(c.value) ? null : {
      validNumber: {
        valid: false,
        errorMsg: '只允许输入数字'
      }
    });
  }
}
