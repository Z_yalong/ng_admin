import {Injectable} from "@angular/core";
import {HttpService} from "../service/http.service";

export const environment = {
  production: true
};


@Injectable()
export class ApiService {
  constructor(private http: HttpService) {
  }

  login = {
    login: (option) => this.http.post("/login", option)
  }
  infomation = {
    infomation: () => this.http.get("/infomation"),
    audit:(option) => this.http.post("/infomationAudit",option)
  }
  works = {
    list:() => this.http.get("/works"),
    add:(option) => this.http.post("/worksAdd",option),
    del:(option) => this.http.post("/worksDel",option)
  }
  worksList = {
    list:(option) => this.http.get(`/webWorksList?type=${option}`),
    add:(option) => this.http.post("/webWorksListAdd",option),
    del:(option) => this.http.post("/webWorksListDel",option)
  }
  imageUpload = {
    uploadToken:() => this.http.get("/uploadToken"),
  }
}
