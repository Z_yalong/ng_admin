// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {Injectable} from "@angular/core";
import {HttpService} from "../service/http.service";

export const environment = {
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

@Injectable()
export class  ApiService {
  constructor(private http:HttpService){}

  login = {
    login:(option) => this.http.post("/login",option)
  }
  infomation = {
    infomation:() => this.http.get("/infomation"),
    audit:(option) => this.http.post("/infomationAudit",option)
  }
  works = {
    list:() => this.http.get("/works"),
    add:(option) => this.http.post("/worksAdd",option),
    del:(option) => this.http.post("/worksDel",option)
  }
  worksList = {
    list:(option) => this.http.get(`/webWorksList?type=${option}`),
    add:(option) => this.http.post("/webWorksListAdd",option),
    del:(option) => this.http.post("/webWorksListDel",option)
  }
  imageUpload = {
    uploadToken:() => this.http.get("/uploadToken"),
  }
}
