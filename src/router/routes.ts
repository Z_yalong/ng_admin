import {Routes} from '@angular/router'
import {LoginComponent} from "../app/login/login.component";


export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'login', pathMatch: "full"},
  {path: 'home', loadChildren: 'src/app/layout/home/home.module#HomeModule'},
  {path: 'login', component: LoginComponent}
]
