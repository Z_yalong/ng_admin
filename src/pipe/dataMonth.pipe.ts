import {Pipe, PipeTransform} from "@angular/core";


@Pipe({
  name:'MonthPipe'
})
export class DataMonthPipe  implements PipeTransform{

  transform(value:any,args?:any) :any {
    let data = new Date(value)
    let year = data.getFullYear();  // 获取完整的年份(4位,1970)
    let month = data.getMonth();  // 获取月份(0-11,0代表1月,用的时候记得加上1)
    return year + '年' + month +'月'
  }

}
