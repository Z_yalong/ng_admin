import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-nav-header',
  templateUrl: './nav-header.component.html',
  styleUrls: ['./nav-header.component.css']
})
export class NavHeaderComponent implements OnInit {
  Collapsed:boolean = false;

  constructor() {
  }

  @Input()
  get isCollapsed() {
    return this.Collapsed
  } ;

  set isCollapsed(value) {
    this.Collapsed = value
    this.isCollapsedChange.emit(this.Collapsed)
  }

  @Output() isCollapsedChange: EventEmitter<boolean> = new EventEmitter()

  ngOnInit() {

  }

  toggleCollapsed(): void {
    this.isCollapsed = !this.isCollapsed;
  }
}
