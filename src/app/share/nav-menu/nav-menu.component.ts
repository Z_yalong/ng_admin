import { Component, OnInit ,Input } from '@angular/core';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  constructor() { }
  @Input() isCollapsed:boolean= false;

  navHeight = 500 ;
  ngOnInit() {
    this.navHeight = document.documentElement.clientHeight || document.body.clientHeight;
  }
  menuClick(e){
    console.log(e);
  }
}
