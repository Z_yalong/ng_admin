import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, zh_CN } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import {rootRouterConfig} from "../router/routes";
import {RouterModule} from "@angular/router";
import { LoginComponent } from './login/login.component';
import {HttpService} from "../service/http.service";
import {ApiService} from "../environments/environment";
import {BlockUiService} from "../service/block-ui.service";

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    RouterModule.forRoot(rootRouterConfig,{useHash: true})
  ],
  providers: [
    { provide: NZ_I18N, useValue: zh_CN },
    BlockUiService,
    HttpService,
    ApiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
