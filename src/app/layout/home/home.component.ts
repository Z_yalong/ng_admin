import {Component, OnInit} from '@angular/core';
import {HttpService} from "../../../service/http.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public http :HttpService) {
  }

  isCollapsed = false
  clientH: number
  isSpinning: boolean;

  ngOnInit() {
    this.clientH = document.documentElement.clientHeight || document.body.clientHeight
    this.http.currentUser.subscribe (data => {
      this.isSpinning = data
    })
  }
}
