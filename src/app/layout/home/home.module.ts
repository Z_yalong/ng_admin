import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {RouterModule, Routes} from "@angular/router";
import {NgZorroAntdModule} from "ng-zorro-antd";
import {NavHeaderComponent} from "../../share/nav-header/nav-header.component";
import {WenlcomeComponent} from './wenlcome/wenlcome.component';
import {NavMenuComponent} from "../../share/nav-menu/nav-menu.component";
import {HuihuiWEBModule} from "./huihui-web/huihui-web.module";
import {FormsModule} from "@angular/forms";
import {HttpService} from "../../../service/http.service";

const routes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [
      {path: '', redirectTo: 'welcome',},
      {path: 'welcome', component: WenlcomeComponent},
      {path: 'H-web', loadChildren:'src/app/layout/home/huihui-web/huihui-web.module#HuihuiWEBModule'}
    ]
  },

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgZorroAntdModule,
  ],
  declarations: [
    HomeComponent,
    NavHeaderComponent,
    NavMenuComponent,
    WenlcomeComponent,
  ],
  providers:[
    HttpService
  ]
})
export class HomeModule {
}
