import {Component, OnInit} from '@angular/core';
import {NzMessageService, NzModalService} from "ng-zorro-antd";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../../../../../environments/environment";

@Component({
  selector: 'app-eduit-work',
  templateUrl: './eduit-work.component.html',
  styleUrls: ['./eduit-work.component.css']
})
export class EduitWorkComponent implements OnInit {

  displayData = [];
  isAudit = false;
  Audit_id = '';
  // 编辑
  visible = false;

  auditForm: FormGroup

  data = []

  constructor(private  modal: NzModalService,
              private message: NzMessageService,
              private fb: FormBuilder,
              private API: ApiService) {
  }

  ngOnInit() {
    this.auditForm = this.fb.group({
      companyNmae: [null, [Validators.required]],
      startTime: [null, [Validators.required]],
      endTime: [null, [Validators.required]],
      description: [null, [Validators.required]],
    })
    this.getData()
  }

  getData() {
    this.API.works.list().then(data => {
      this.data = data
    })
  }

  aduitConfirm(): void {
    let option = this.auditForm.value
    option.isAudit = this.isAudit
    option._id = this.Audit_id || null
    // console.log(this.auditForm.value);
    this.API.works.add(this.auditForm.value).then(data => {
      if (data) {
        let msg = this.isAudit ? "修改成功!" : "添加成功!"
        this.message.success(msg);
        this.aduitClose()
        this.getData()
      }

    })
  }

  aduitClose(): void {
    this.visible = false;
    this.isAudit = false
    this.auditForm.reset()
  }


  audit(data) {
    this.isAudit = true
    this.Audit_id = data._id
    this.auditForm.patchValue({...data})
    this.open()
  }

  open(): void {
    this.visible = true;
  }

  confirm(data): void {
    this.API.works.del({_id: data._id}).then(data => {
      this.message.success('删除成功！');
      this.getData()
    })

  }


  currentPageDataChange($event: Array<{ companyNmae: string; startTime: any; endTime: any; description: string; checked: boolean; disabled: boolean; }>): void {
    this.displayData = $event;
  }


}
