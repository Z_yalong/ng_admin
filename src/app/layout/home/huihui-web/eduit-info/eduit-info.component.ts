import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../../../../../environments/environment";
import {NzMessageService, UploadFile} from "ng-zorro-antd";
import CustomValidators from "../../../../../forms/CustomValidators";

@Component({
  selector: 'app-eduit-info',
  templateUrl: './eduit-info.component.html',
  styleUrls: ['./eduit-info.component.css']
})
export class EduitInfoComponent implements OnInit {
  validateForm: FormGroup;
  infoData:any = {}

  photo = [];

  constructor(private fb: FormBuilder,
              private api:ApiService,
              private message:NzMessageService) {
  }

  ngOnInit() {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      occupation: [null, [Validators.required]],
      school: [null, [Validators.required]],
      age: [null, [Validators.required,CustomValidators.validNumber]],
      serviceAge: [null, [Validators.required,CustomValidators.validNumber]],
      phone: [null, [Validators.required]],
      oneself: [null, [Validators.required]],
    });
    this.api.infomation.infomation().then( data => {
      this.infoData = data
      data.photo.forEach( item => {
        item.oldurl = item.url
        item.url = 'http://qiniu.hasstart.com/' + item.url
      })
      this.photo = data.photo
       this.validateForm.patchValue(data)
    })
    this.api.imageUpload.uploadToken().then(data => {
      this.imageToken.token = data
    })
  }

  submitForm(): void {
    let option ={ ...this.validateForm.value}
    option._id = this.infoData._id


    option.photo = []
    this.photo.forEach(item => {
      option.photo.push({
        uid: item.uid,
        type: item.type,
        size: item.size,
        status: item.status,
        lastModifiedDate: item.lastModifiedDate,
        url: item.oldurl ? item.oldurl :  item.response.key,
      })
    })

    this.api.infomation.audit(option).then( data => {
     if(data){
       this.message.success('修改成功~')
     }
    })
  }

  // 上传图片
  loading = false;
  isAudit = false;
  // 七牛token
  imageToken = {
    token: ''
  }

  previewImage = '';
  previewVisible = false;

  beforeUpload = (file: File) => {

    let fileLatname = file.name.lastIndexOf('.');
    let extension = file.name.substring(fileLatname).toLowerCase();
    let allowed = ['.jpg', '.jpeg', '.gif', '.png', '.bmp'];
    let isJPG = allowed.indexOf(extension) != -1 ? true : false;

    if (!isJPG) {
      this.message.error('仅支持(jpg / jpeg / gif / png / bmp)格式!');
    }
    const isLt2M = file.size / 1024 / 1024 < 1000;
    if (!isLt2M) {
      this.message.error('图片不能大于1G!');
    }
    return isJPG && isLt2M;
  }

  private getBase64(img: File, callback: (img: {}) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange(info: { file: UploadFile }): void {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: string) => {
        this.loading = false;
      });
    }
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }

}
