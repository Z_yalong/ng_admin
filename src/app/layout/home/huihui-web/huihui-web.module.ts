import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EduitTablesComponent} from './eduit-tables/eduit-tables.component';
import {RouterModule, Routes} from "@angular/router";
import {NgZorroAntdModule} from "ng-zorro-antd";
import { EduitInfoComponent } from './eduit-info/eduit-info.component';
import {FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import { EduitWorkComponent } from './eduit-work/eduit-work.component';
import {DataMonthPipe} from "../../../../pipe/dataMonth.pipe";


const routes: Routes = [
  { path: '', redirectTo:'info' , pathMatch:'full'},
  { path: 'info', component: EduitInfoComponent },
  { path: 'work', component: EduitWorkComponent },
  { path: 'tables/:type', component: EduitTablesComponent }

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,

    NgZorroAntdModule,
  ],
  declarations: [
    EduitTablesComponent,
    EduitInfoComponent,
    EduitWorkComponent,
    DataMonthPipe
  ],
  exports:[
    DataMonthPipe
  ]
})
export class HuihuiWEBModule {
}
