import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {NzMessageService, UploadFile} from "ng-zorro-antd";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../../../../../environments/environment";

@Component({
  selector: 'app-eduit-tables',
  templateUrl: './eduit-tables.component.html',
  styleUrls: ['./eduit-tables.component.css']
})
export class EduitTablesComponent implements OnInit {

  constructor(private activeRoute: ActivatedRoute,
              private nzMessageService: NzMessageService,
              private fb: FormBuilder,
              private API: ApiService) {
  }

  worksLists = []
  displayData = [];
  // 标题
  type: number = 1
  title: string;
  titleList = ['', 'UI', '网站', '平面', '绘画']
  // 对话框
  isVisible: boolean = false
  // 表单
  validateForm: FormGroup;
  // 上传图片
  loading = false;
  isAudit = false;
  // 七牛token
  imageToken = {
    token: ''
  }

  // 上传图片列表

  imgSmall = [];
  imgLarge = [];
  _id = null;

  previewImage = '';
  previewVisible = false;

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      this.type = params.type
      this.title = this.titleList[this.type]
      this.getDataList()
    })

    this.validateForm = this.fb.group({
      worksNmae: [null, [Validators.required]],
      desSort: [null, [Validators.required]],
      desLang: [null, [Validators.required]],
    })

  }


  audit(data) {
    this.getToken()
    this.isVisible = true
    this.isAudit = true
    this.validateForm.patchValue(data)
    this._id = data._id


    this.imgSmall = data.imgSmall
    this.imgLarge = data.imgLarge
  }

  auditConfirm() {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (this.validateForm.invalid) {
      return false
    }
    if (this.imgSmall.length < 0) {
      this.nzMessageService.error('请上传小图!')
      return false
    }
    if (this.imgLarge.length < 0) {
      this.nzMessageService.error('请上传大图!')
      return false
    }


    let option = this.validateForm.value
    option.isAudit = this.isAudit
    option.type = this.type
    if (this._id) {
      option._id = this._id
    }

    option.imgSmall = []
    this.imgSmall.forEach(item => {
      option.imgSmall.push({
        uid: item.uid,
        type: item.type,
        size: item.size,
        status: item.status,
        lastModifiedDate: item.lastModifiedDate,
        url: item.oldurl ? item.oldurl : item.response.key,
      })
    })
    option.imgLarge = []
    this.imgLarge.forEach(item => {
      option.imgLarge.push({
        uid: item.uid,
        type: item.type,
        size: item.size,
        status: item.status,
        lastModifiedDate: item.lastModifiedDate,
        url: item.oldurl ? item.oldurl :  item.response.key,
      })
    })

    this.API.worksList.add(option).then(data => {
      let msg = this.isAudit ? "修改成功!" : "添加成功!"
      this.nzMessageService.success(msg);
      this.auditCancel()
      this.getDataList()
    })
  }

  auditCancel() {
    this.isVisible = false
    this.isAudit = false
    this._id = null
    this.imgSmall = []
    this.imgLarge = []
    this.validateForm.reset()
  }

  del(data): void {
    this.API.worksList.del({_id: data._id}).then(data => {
      this.nzMessageService.success('删除成功！');
      this.getDataList()
    })

  }


  // 上传图片
  beforeUpload = (file: File) => {
    let fileLatname = file.name.lastIndexOf('.');
    let extension = file.name.substring(fileLatname).toLowerCase();
    let allowed = ['.jpg', '.jpeg', '.gif', '.png', '.bmp'];
    let isJPG = allowed.indexOf(extension) != -1 ? true : false;

    if (!isJPG) {
      this.nzMessageService.error('仅支持(jpg / jpeg / gif / png / bmp)格式!');
    }
    const isLt2M = file.size / 1024 / 1024 < 1000;
    if (!isLt2M) {
      this.nzMessageService.error('图片不能大于1G!');
    }
    return isJPG && isLt2M;
  }

  private getBase64(img: File, callback: (img: {}) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange(info: { file: UploadFile }): void {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: string) => {
        this.loading = false;
      });
    }
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }


  add() {
    this.getToken()
    this.isVisible = true
    this.isAudit = false
  }


  getToken() {
    this.API.imageUpload.uploadToken().then(data => {
      this.imageToken.token = data
    })
  }

  getDataList() {
    this.API.worksList.list(this.type).then(data => {
      data.forEach(val => {
        val.imgSmall.forEach( item => {
          item.oldurl = item.url
          item.url = 'http://qiniu.hasstart.com/' + item.url
        })

        val.imgLarge.forEach( item => {
          item.oldurl = item.url
          item.url = 'http://qiniu.hasstart.com/' + item.url
        })
      })

      this.worksLists = data
    })
  }

  currentPageDataChange($event: Array<{ worksNmae: string; desSort: string; desLang: string; imgSmall: any; imgLarge: any; checked: boolean; disabled: boolean; }>): void {
    this.displayData = $event;
  }

}
